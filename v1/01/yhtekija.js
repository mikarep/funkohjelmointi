var gcd = function(a, b) {
    if ( ! b) {
        return a;
    }

    return gcd(b, a % b);
};
console.log(gcd(102, 68));

var kjl = function(a, b) {
    if (gcd(a, b) == 1){
        return true;
    }
    return false;
};
console.log(kjl(35, 18));
console.log(kjl(102, 68));