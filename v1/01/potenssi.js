//var potenssi = function(a, b) {
//    if (b == 0){
//        return 1;
//    }
//    return Math.pow(a, b);
//};
//console.log(potenssi(2, 3));

function potenssi(a, b){
   function potenssiHelper(a, b){
       if(b == 0){
           return 1;
       } else {
           return Math.pow(a, b);
       }
   } 
   return potenssiHelper(a, b);
}

var tulos = potenssi(2, 3);

console.log(tulos);