var reverse = function(a) {
    if (!a.length) return a;
     return reverse(a.slice(1)).concat(a[0]);
};
console.log(reverse([0, 1, 2, 3]));
