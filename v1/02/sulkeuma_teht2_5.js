'use strict';

let f, g;
function foo() {
  let x;
  f = function() { return ++x; };
  g = function() { return --x; };
  x = 1;
  console.log('inside foo, call to f(): ' + f());
}
foo();  
console.log('call to g(): ' + g()); 
console.log('call to f(): ' + f()); 


var Moduuli = (function(){
  let y = 1;
  return {
    kasvata: function(){return ++y;},
    vahenna: function(){return --y;}
  }
})();

console.log(Moduuli.kasvata());
console.log(Moduuli.vahenna());
