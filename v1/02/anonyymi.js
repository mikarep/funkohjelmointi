'use strict'

const f = function () {
    return function (x) {
		return x+1;
	 }
	}(); // Huomaa funktion kutsu!

let tulos = f(3);

console.log(tulos);

const x = function() {
	return function(a, b) {
		if (a > b){
			return 1;
		} else if (a < b){
			return -1;
		} else if (a == b){
			return 0;
		}
	}
}();

let vertaus = x(2, 1);

var a = [-10, -1, 6, 7, 12, 13, 20, 15, 7, 4, 1, -5]; //vuoden 2016 lämpötilat
var b = [-11, -4, 6, 6, 10, 15, 24, 17, 6, 2, 0, 0]; // vuoden 2015 lämpötilat
console.log(vertaus);
function lampotila(nimi, a, b){
	var i = 0;
	var lasku = 0;
	while (i <= 12){
		var x = a[i];
		var y = b[i];
		var vertaus = nimi(x, y);
		if (vertaus == 1){
			lasku++;
		}
		i++;
	}
	return lasku;
}
console.log(lampotila(x, a, b));