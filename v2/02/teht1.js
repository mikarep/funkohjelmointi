function laskePisteet(pituus){
    return function(lisapisteet){
        if (lisapisteet == 1.8 && pituus <= 99){
            var a = pituus - 75;
            pituus = pituus + (a * lisapisteet);
            return pituus;
        } else if (lisapisteet == 2.0 && pituus >= 100) {
            var b = pituus - 100;
            pituus = pituus + (b * lisapisteet);
            return pituus;
        }
    }
}

console.log(laskePisteet(98)(1.8));