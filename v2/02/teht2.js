const Auto = (function(){
    
    const suojattu = new WeakMap();
    
    class Auto{
        constructor(p_tankki, p_matkamittari){
            suojattu.set(this, {matkamittari: p_matkamittari});
            this.tankki = p_tankki;
        }
        
        getMatkamitari(){
            return suojattu.get(this).matkamittari;
            
        }
        getTankki() {
            return this.tankki;
            
        }
        
        aja() {
            suojattu.get(this).matkamittari++, this.tankki--;
        }
        
        lisaaTankkiin(x) {
            this.tankki += x;
        }
        
        getMap() {
            return suojattu;
        }
    }
    
    return Auto;
})

const auto = new Auto(30, 0);
auto.aja();
auto.aja();
auto.aja();
auto.aja();
auto.lisaaTankkiin(5);
console.log(auto.getMatkamitari());
console.log(auto.getTankki());
