(ns test-lein.core
  (:gen-class))

;(defn -main
;  "I don't do a whole lot ... yet."
;  [& args]
;  (println "Hello, World!"))


(defn square [x]
  (* x x))

(defn karkausvuosi? [x] (and (= 0 (rem x 4)) (or (= 0 (rem x 400)) (not= 0 (rem x 100)))))
;(fd [name] (str "Tervetuloa Tylypahkaan " name))


;{:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}}