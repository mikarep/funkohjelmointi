(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Anna luku(>0)")
  (flush)
  (let [line (read-line)
        value (try
              (Integer/parseInt line)
              (catch NumberFormatException e line))]
  (if (> value 0) 
      (if (= 0 (rem value 2)) 
          (println "Parillinen") 
          (println "Pariton")) 
      (println "Virhe"))))
