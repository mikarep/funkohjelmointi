import java.util.function.DoubleUnaryOperator;

public class Makihyppy {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
        return (double pisteet) -> pisteet + ((pisteet - kPiste) * lisapisteet);
    }
        
    public static void main(String[] args) {

       
       DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 1.8);
       
       System.out.println(/* Kutsu tähän */normaaliLahti.applyAsDouble(80)); 
          
    }
    
}