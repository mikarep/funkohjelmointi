package menu;
import java.util.*;
import java.util.stream.*;

public class Teht1{
    
    interface toCelsius{
        double lmuutos(double fahrenheit);
    }
    
    interface alue{
        double amuutos(double area);
    }
    
    public static void main(String...args){
        toCelsius FahToCel = (fahrenheit) -> (5 / 9) * (fahrenheit - 32);
        alue muutosArea = (area) -> Math.PI * area * area;
        
       
        System.out.println("Lämpömuutos: " + FahToCel.lmuutos(32));
        System.out.println("Aluemuutos: " + muutosArea.amuutos(20));
        
    }
    
    
}