package menu;

import java.util.*;
import java.util.stream.*;
import static Hallinta.Dish.dishTags;
import static Hallinta.Dish.menu;
import static Hallinta.Dish.Type;
import static Hallinta.Dish.Type.MEAT;
import static Hallinta.Dish.Type.FISH;
import static Hallinta.Dish.Type.OTHER;
import static java.util.stream.Collectors.toList;


public class teht2{
    public static void main(String...args){
        
        List<Type> meat = menu.stream()
                        .filter(dish -> dish.getType() == MEAT)
                        .map(dish -> dish.getType())
                        .collect(toList());
                        
        
        int meatcount = meat.size();
        
        List<Type> fish = menu.stream()
                        .filter(dish -> dish.getType() == FISH)
                        .map(dish -> dish.getType())
                        .collect(toList());
                        
        
        int fishcount = fish.size();
        
        List<Type> other = menu.stream()
                        .filter(dish -> dish.getType() == OTHER)
                        .map(dish -> dish.getType())
                        .collect(toList());
                        
        
        int othercount = other.size();
        
        System.out.println("Meat type: " + meatcount);
        System.out.println("Fish type: " + fishcount);
        System.out.println("Other type: " + othercount);
        
    }
        
}