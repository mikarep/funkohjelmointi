(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  
  
  (println (/ 
              (reduce + 
                      (filter #(> % 0) [0, -1, 4, 5, 10, 17, 8, 5, 4, -2, -1, 0, -5, 3, 4, 10, 15, 2, 1, -1, -5, -3 , -2, -10])) 
                    (count (filter #(> % 0) [0, -1, 4, 5, 10, 17, 8, 5, 4, -2, -1, 0, -5, 3, 4, 10, 15, 2, 1, -1, -5, -3 , -2, -10]))))
    
    
  )



;(defn tulosta [lampotilat] (filter #(> (:lampotila %) 0) lampotilat))

;(filter #(> (:lampotila %) 0) lampotilat)

(def lampotilat
  [{:year 2015 :month 1 :lampotila -5}
   {:year 2015 :month 2 :lampotila -4}
   {:year 2015 :month 3 :lampotila 3}
   {:year 2015 :month 4 :lampotila 3}
   {:year 2015 :month 5 :lampotila 4}
   {:year 2015 :month 6 :lampotila 10}
   {:year 2015 :month 7 :lampotila 21}
   {:year 2015 :month 8 :lampotila 15}
   {:year 2015 :month 9 :lampotila 10}
   {:year 2015 :month 10 :lampotila 0}
   {:year 2015 :month 11 :lampotila -1}
   {:year 2015 :month 12 :lampotila -6}
   {:year 2016 :month 1 :lampotila -10}
   {:year 2016 :month 2 :lampotila -15}
   {:year 2016 :month 3 :lampotila -3}
   {:year 2016 :month 4 :lampotila 2}
   {:year 2016 :month 5 :lampotila 5}
   {:year 2016 :month 6 :lampotila 10}
   {:year 2016 :month 7 :lampotila 15}
   {:year 2016 :month 8 :lampotila 10}
   {:year 2016 :month 9 :lampotila 3}
   {:year 2016 :month 10 :lampotila -1}
   {:year 2016 :month 11 :lampotila -4}
   {:year 2016 :month 12 :lampotila -10}])