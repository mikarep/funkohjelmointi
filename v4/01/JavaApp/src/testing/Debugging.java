package testing;


import java.util.*;
import static java.util.stream.Collectors.toList;

public class Debugging{
    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(12, 2), new Point(2, 2));
        points.stream().map(p -> p.getX()).forEach(System.out::println);
        
        List<Point> points2 = Arrays.asList(new Point(5,5), new Point(10,5));
        List<Point> expectedPoints = Arrays.asList(new Point(15,5), new Point(20,5));
        List<Point> newPoints = moveAllPointsRightBy(points2, 10);
        
        expectedPoints.stream().map(p -> p.getX()).forEach(System.out::println);
        newPoints.stream().map(p -> p.getX()).forEach(System.out::println);
    }


    private static class Point{
        private int x;
        private int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
        
        public Point moveRightBy(int x) {
            return new Point(this.x + x, this.y);
        }
        
        
        
    }
    
    
    public static List<Point> moveAllPointsRightBy( List<Point> points, int x) {
        return points.stream().map(p -> p.moveRightBy(x)).collect(toList());
    }
}
